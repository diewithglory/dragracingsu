// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DragRacingShiftUpGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DRAGRACINGSHIFTUP_API ADragRacingShiftUpGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
